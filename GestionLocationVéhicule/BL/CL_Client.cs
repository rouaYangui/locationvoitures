﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionLocationVéhicule.BL
{
    class CL_Client
    {
        private dbLocationContext db = new dbLocationContext();
        private Client C; // table Client 
        public bool Ajouter_Client(string Nom, string prenom, string Adresse, string Telephone, string Cin, string Permis)
        {
            C = new Client(); // nouveau Client roua & molka
            C.Nom_client = Nom;
            
            C.Prenom_Client = prenom;
            C.Adresse_Client = Adresse;
            C.tel = Telephone;
            C.CIN = Cin;
            C.permis = Permis;
            //verifier si le nom et le prenom existe déj) dans la base de donnée 
            if (db.Clients.SingleOrDefault(s => s.CIN == Cin) == null) // si n'existe pas
            {
                db.Clients.Add(C);// ajouter dans la table client
                db.SaveChanges();// save dans BD
                return true;
            }
            else // si existe dans le BD
            {
                return false;
            }
        }
        public void modifier_client(int id, string Nom, string prenom, string Adresse, string Telephone, string Cin, string Permis)
        {
            C = new Client();
            C = db.Clients.SingleOrDefault(s => s.ID_Client == id); // verifier si id de client est existe 
            if (C != null)//si existe 
            {
                C.Nom_client = Nom; // nv nom 
                C.Prenom_Client = prenom; // nv prenom 
                C.Adresse_Client = Adresse;
                C.tel = Telephone;
                C.CIN = Cin;
                C.permis = Permis;
                db.SaveChanges(); // save nv info dans bd
               



            }
        }
        public void suprimer_Client(int id)
        {
            C = new Client();
            C = db.Clients.SingleOrDefault(s => s.ID_Client == id);
            db.Clients.Remove(C);
            db.SaveChanges();


        }

    }
}