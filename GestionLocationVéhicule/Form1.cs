﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionLocationVéhicule
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!pnlafiche.Controls.Contains(PL.UserControlClient.Instance))
            {
                pnlafiche.Controls.Add(PL.UserControlClient.Instance);
                PL.UserControlClient.Instance.Dock = DockStyle.Fill;
                PL.UserControlClient.Instance.BringToFront();

            }
            else
            {
                PL.UserControlClient.Instance.BringToFront();


            }
        }

        private void btnV_Click(object sender, EventArgs e)
        {

            if (!pnlafiche.Controls.Contains(PL.UserControlVehicule.Instance))
            {
                pnlafiche.Controls.Add(PL.UserControlVehicule.Instance);
                PL.UserControlVehicule.Instance.Dock = DockStyle.Fill;
                PL.UserControlVehicule.Instance.BringToFront();
            }
            else
            {
                PL.UserControlVehicule.Instance.BringToFront();
            }
        }
    }
}
