﻿
namespace GestionLocationVéhicule.PL
{
    partial class FRM_Vehicule_Ajout_Modifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textkilo = new System.Windows.Forms.TextBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textprix = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textmodele = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.textmarque = new System.Windows.Forms.TextBox();
            this.LBtitre = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnquite = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textmatricule = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imagebox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnEnregistre = new System.Windows.Forms.Button();
            this.btnActualiser = new System.Windows.Forms.Button();
            this.btnParcourire = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnquite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(453, 280);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 1);
            this.panel1.TabIndex = 73;
            // 
            // textkilo
            // 
            this.textkilo.BackColor = System.Drawing.Color.GhostWhite;
            this.textkilo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textkilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textkilo.ForeColor = System.Drawing.Color.Gray;
            this.textkilo.Location = new System.Drawing.Point(453, 251);
            this.textkilo.Margin = new System.Windows.Forms.Padding(2);
            this.textkilo.Multiline = true;
            this.textkilo.Name = "textkilo";
            this.textkilo.Size = new System.Drawing.Size(204, 31);
            this.textkilo.TabIndex = 72;
            this.textkilo.Text = "Kilometrage";
            this.textkilo.Enter += new System.EventHandler(this.textkilo_Enter);
            this.textkilo.Leave += new System.EventHandler(this.textkilo_Leave);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox8.Location = new System.Drawing.Point(420, 251);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(29, 31);
            this.pictureBox8.TabIndex = 71;
            this.pictureBox8.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gray;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel7.Location = new System.Drawing.Point(457, 340);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(188, 1);
            this.panel7.TabIndex = 70;
            // 
            // textprix
            // 
            this.textprix.BackColor = System.Drawing.Color.GhostWhite;
            this.textprix.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textprix.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textprix.ForeColor = System.Drawing.Color.Gray;
            this.textprix.Location = new System.Drawing.Point(453, 311);
            this.textprix.Margin = new System.Windows.Forms.Padding(2);
            this.textprix.Multiline = true;
            this.textprix.Name = "textprix";
            this.textprix.Size = new System.Drawing.Size(204, 31);
            this.textprix.TabIndex = 68;
            this.textprix.Text = "Prix/Jour";
            this.textprix.Enter += new System.EventHandler(this.textprix_Enter);
            this.textprix.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textprix_KeyPress);
            this.textprix.Leave += new System.EventHandler(this.textprix_Leave);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Gray;
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel6.Location = new System.Drawing.Point(458, 223);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(188, 1);
            this.panel6.TabIndex = 67;
            // 
            // textmodele
            // 
            this.textmodele.BackColor = System.Drawing.Color.GhostWhite;
            this.textmodele.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textmodele.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textmodele.ForeColor = System.Drawing.Color.Gray;
            this.textmodele.Location = new System.Drawing.Point(454, 191);
            this.textmodele.Margin = new System.Windows.Forms.Padding(2);
            this.textmodele.Multiline = true;
            this.textmodele.Name = "textmodele";
            this.textmodele.Size = new System.Drawing.Size(204, 31);
            this.textmodele.TabIndex = 65;
            this.textmodele.Text = "Modele";
            this.textmodele.Enter += new System.EventHandler(this.textmodele_Enter);
            this.textmodele.Leave += new System.EventHandler(this.textmodele_Leave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel5.Location = new System.Drawing.Point(453, 166);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(188, 1);
            this.panel5.TabIndex = 64;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox2.Location = new System.Drawing.Point(420, 137);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 31);
            this.pictureBox2.TabIndex = 63;
            this.pictureBox2.TabStop = false;
            // 
            // textmarque
            // 
            this.textmarque.BackColor = System.Drawing.Color.GhostWhite;
            this.textmarque.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textmarque.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textmarque.ForeColor = System.Drawing.Color.Gray;
            this.textmarque.Location = new System.Drawing.Point(453, 137);
            this.textmarque.Margin = new System.Windows.Forms.Padding(2);
            this.textmarque.Multiline = true;
            this.textmarque.Name = "textmarque";
            this.textmarque.Size = new System.Drawing.Size(204, 31);
            this.textmarque.TabIndex = 62;
            this.textmarque.Text = "Marque voiture";
            this.textmarque.Enter += new System.EventHandler(this.textmarque_Enter);
            this.textmarque.Leave += new System.EventHandler(this.textmarque_Leave);
            // 
            // LBtitre
            // 
            this.LBtitre.AutoSize = true;
            this.LBtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitre.ForeColor = System.Drawing.Color.White;
            this.LBtitre.Location = new System.Drawing.Point(11, 7);
            this.LBtitre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LBtitre.Name = "LBtitre";
            this.LBtitre.Size = new System.Drawing.Size(150, 24);
            this.LBtitre.TabIndex = 61;
            this.LBtitre.Text = "Ajouter Voiture";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.panel3.BackColor = System.Drawing.Color.SeaGreen;
            this.panel3.Controls.Add(this.btnquite);
            this.panel3.Controls.Add(this.LBtitre);
            this.panel3.Location = new System.Drawing.Point(-2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(676, 42);
            this.panel3.TabIndex = 77;
            // 
            // btnquite
            // 
            this.btnquite.Image = global::GestionLocationVéhicule.Properties.Resources.Button_Delete_icon1;
            this.btnquite.Location = new System.Drawing.Point(634, 7);
            this.btnquite.Margin = new System.Windows.Forms.Padding(2);
            this.btnquite.Name = "btnquite";
            this.btnquite.Size = new System.Drawing.Size(31, 31);
            this.btnquite.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnquite.TabIndex = 62;
            this.btnquite.TabStop = false;
            this.btnquite.Click += new System.EventHandler(this.btnquite_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel4.Location = new System.Drawing.Point(453, 106);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(188, 1);
            this.panel4.TabIndex = 80;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox5.Location = new System.Drawing.Point(420, 77);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(29, 31);
            this.pictureBox5.TabIndex = 79;
            this.pictureBox5.TabStop = false;
            // 
            // textmatricule
            // 
            this.textmatricule.BackColor = System.Drawing.Color.GhostWhite;
            this.textmatricule.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textmatricule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textmatricule.ForeColor = System.Drawing.Color.Gray;
            this.textmatricule.Location = new System.Drawing.Point(453, 77);
            this.textmatricule.Margin = new System.Windows.Forms.Padding(2);
            this.textmatricule.Multiline = true;
            this.textmatricule.Name = "textmatricule";
            this.textmatricule.Size = new System.Drawing.Size(204, 31);
            this.textmatricule.TabIndex = 78;
            this.textmatricule.Text = "Matricule voiture";
            this.textmatricule.Enter += new System.EventHandler(this.textmatricule_Enter);
            this.textmatricule.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textmatricule_KeyPress);
            this.textmatricule.Leave += new System.EventHandler(this.textmatricule_Leave_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox3.Location = new System.Drawing.Point(420, 191);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 31);
            this.pictureBox3.TabIndex = 66;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox1.Location = new System.Drawing.Point(415, 311);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 31);
            this.pictureBox1.TabIndex = 81;
            this.pictureBox1.TabStop = false;
            // 
            // imagebox
            // 
            this.imagebox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imagebox.Location = new System.Drawing.Point(153, 77);
            this.imagebox.Name = "imagebox";
            this.imagebox.Size = new System.Drawing.Size(253, 188);
            this.imagebox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imagebox.TabIndex = 82;
            this.imagebox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 20);
            this.label1.TabIndex = 83;
            this.label1.Text = "&Image : ";
            // 
            // btnEnregistre
            // 
            this.btnEnregistre.BackColor = System.Drawing.Color.SeaGreen;
            this.btnEnregistre.FlatAppearance.BorderSize = 0;
            this.btnEnregistre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistre.ForeColor = System.Drawing.Color.White;
            this.btnEnregistre.Location = new System.Drawing.Point(388, 388);
            this.btnEnregistre.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnregistre.Name = "btnEnregistre";
            this.btnEnregistre.Size = new System.Drawing.Size(140, 39);
            this.btnEnregistre.TabIndex = 85;
            this.btnEnregistre.Text = "Enregistre ";
            this.btnEnregistre.UseVisualStyleBackColor = false;
            this.btnEnregistre.Click += new System.EventHandler(this.btnEnregistre_Click);
            // 
            // btnActualiser
            // 
            this.btnActualiser.BackColor = System.Drawing.Color.Teal;
            this.btnActualiser.FlatAppearance.BorderSize = 0;
            this.btnActualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualiser.ForeColor = System.Drawing.Color.White;
            this.btnActualiser.Location = new System.Drawing.Point(223, 388);
            this.btnActualiser.Margin = new System.Windows.Forms.Padding(2);
            this.btnActualiser.Name = "btnActualiser";
            this.btnActualiser.Size = new System.Drawing.Size(125, 39);
            this.btnActualiser.TabIndex = 84;
            this.btnActualiser.Text = "Actualiser";
            this.btnActualiser.UseVisualStyleBackColor = false;
            this.btnActualiser.Click += new System.EventHandler(this.btnActualiser_Click);
            // 
            // btnParcourire
            // 
            this.btnParcourire.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnParcourire.FlatAppearance.BorderSize = 0;
            this.btnParcourire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParcourire.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParcourire.ForeColor = System.Drawing.Color.White;
            this.btnParcourire.Location = new System.Drawing.Point(11, 130);
            this.btnParcourire.Margin = new System.Windows.Forms.Padding(2);
            this.btnParcourire.Name = "btnParcourire";
            this.btnParcourire.Size = new System.Drawing.Size(127, 33);
            this.btnParcourire.TabIndex = 86;
            this.btnParcourire.Text = "Parcourire..";
            this.btnParcourire.UseVisualStyleBackColor = false;
            this.btnParcourire.Click += new System.EventHandler(this.btnParcourire_Click);
            // 
            // FRM_Vehicule_Ajout_Modifier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(674, 450);
            this.Controls.Add(this.btnParcourire);
            this.Controls.Add(this.btnEnregistre);
            this.Controls.Add(this.btnActualiser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imagebox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.textmatricule);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textkilo);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.textprix);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textmodele);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.textmarque);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FRM_Vehicule_Ajout_Modifier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FRM_Vehicule_Ajout_Modifier";
            this.Load += new System.EventHandler(this.FRM_Vehicule_Ajout_Modifier_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnquite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imagebox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox textkilo;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.TextBox textprix;
        private System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.TextBox textmodele;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox textmarque;
        public System.Windows.Forms.Label LBtitre;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox5;
        public System.Windows.Forms.TextBox textmatricule;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox imagebox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnEnregistre;
        public System.Windows.Forms.Button btnActualiser;
        public System.Windows.Forms.Button btnParcourire;
        private System.Windows.Forms.PictureBox btnquite;
    }
}