﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionLocationVéhicule.PL
{
    public partial class FRM_Connexion : Form
    {
        private dbLocationContext bd;
        BL.Login C = new BL.Login();
        public FRM_Connexion()
        {
            InitializeComponent();
            bd = new dbLocationContext();

        }
        string testobligatoire()
        {
            if (textNomUtilisateur.Text.Equals(""))
            {
                textNomUtilisateur.Focus();
                return "Entrer votre nom";
            }
            if (textPass.Text.Equals(""))
            {
                textPass.Focus();
                return "Entrer votre Mode de passe ";
            }
            return null;
        }

        private void FRM_Connexion_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnConnexion_Click(object sender, EventArgs e)
        {
            if (testobligatoire() == null)
            {
                if (C.ConnexionValide(bd, textNomUtilisateur.Text, textPass.Text) == true)
                {
                    MessageBox.Show(" Connexion réussi", "Login", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    this.Hide();
                    Form1 F = new Form1();
                    F.Show();

                }
                else
                {
                    MessageBox.Show("Connexion échoué ", "Login", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
            }
            else
                MessageBox.Show(testobligatoire(), "obligatoire", MessageBoxButtons.OK);
        }

        private void showpassword_Click(object sender, EventArgs e)
        {
            textPass.PasswordChar = textPass.PasswordChar == '*' ? '\0' : '*';
        }
    }
}

