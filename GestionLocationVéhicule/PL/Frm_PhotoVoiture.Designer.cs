﻿
namespace GestionLocationVéhicule.PL
{
    partial class Frm_PhotoVoiture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.VoitureNom = new System.Windows.Forms.Label();
            this.btnannuller = new System.Windows.Forms.PictureBox();
            this.ImageVoiture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageVoiture)).BeginInit();
            this.SuspendLayout();
            // 
            // VoitureNom
            // 
            this.VoitureNom.AutoSize = true;
            this.VoitureNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VoitureNom.Location = new System.Drawing.Point(275, 29);
            this.VoitureNom.Name = "VoitureNom";
            this.VoitureNom.Size = new System.Drawing.Size(76, 25);
            this.VoitureNom.TabIndex = 34;
            this.VoitureNom.Text = "label1";
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::GestionLocationVéhicule.Properties.Resources.Button_Delete_icon1;
            this.btnannuller.Location = new System.Drawing.Point(632, 11);
            this.btnannuller.Margin = new System.Windows.Forms.Padding(2);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(31, 31);
            this.btnannuller.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnannuller.TabIndex = 33;
            this.btnannuller.TabStop = false;
            // 
            // ImageVoiture
            // 
            this.ImageVoiture.BackColor = System.Drawing.Color.LightGray;
            this.ImageVoiture.Location = new System.Drawing.Point(12, 74);
            this.ImageVoiture.Name = "ImageVoiture";
            this.ImageVoiture.Size = new System.Drawing.Size(650, 364);
            this.ImageVoiture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImageVoiture.TabIndex = 0;
            this.ImageVoiture.TabStop = false;
            this.ImageVoiture.Click += new System.EventHandler(this.ImageVoiture_Click);
            // 
            // Frm_PhotoVoiture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(674, 450);
            this.Controls.Add(this.VoitureNom);
            this.Controls.Add(this.btnannuller);
            this.Controls.Add(this.ImageVoiture);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_PhotoVoiture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_PhotoVoiture";
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageVoiture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox btnannuller;
        public System.Windows.Forms.PictureBox ImageVoiture;
        public System.Windows.Forms.Label VoitureNom;
    }
}