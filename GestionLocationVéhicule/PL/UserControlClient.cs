﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionLocationVéhicule.PL
{
    public partial class UserControlClient : UserControl
    {
        private dbLocationContext bd;
        private static UserControlClient UserClient;
        // Creer un instance controle 
        public static UserControlClient Instance
        {
            get
            {
                if (UserClient == null)
                {
                    UserClient = new UserControlClient();
                }
                return UserClient;
            }
        }
        public UserControlClient()
        {
            InitializeComponent();
            bd = new dbLocationContext();
            // desactiver textbox de recherche 
            textBoxRecherche.Enabled = false;

        }

        private void btnAjouterClient_Click(object sender, EventArgs e)
        {
            PL.FRM_Client_Ajouter_Modifier frmclient = new FRM_Client_Ajouter_Modifier(this);
            frmclient.ShowDialog();

        }
        public void Actualiserdatagrid()
        {
            bd = new dbLocationContext();
            dvgClient.Rows.Clear(); // valider datagrid view 
            foreach (var S in bd.Clients)
            {   // ajouter les lignes dans datagrid 
                dvgClient.Rows.Add(false, S.ID_Client, S.CIN, S.Nom_client, S.Prenom_Client, S.Adresse_Client, S.tel, S.permis);
            }
        }
        public string SelectVerif()
        {

            int Nombreligneselect = 0;

            for (int i = 0; i < dvgClient.Rows.Count; i++)
            {
                object value = dvgClient.Rows[i].Cells[0].Value;

                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    Nombreligneselect++; // nombreligneselet +1 

                }
            }
            if (Nombreligneselect == 0)
            {
                return " Selectionner le client que vous-voulez modifier ";

            }
            if (Nombreligneselect > 1)
            {
                return " Selectionner seulement 1 seul client pour modifier ";

            }
            return null;

        }

        private void textBoxRecherche_Enter(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "Recherche")
            {
                textBoxRecherche.Text = "";
                textBoxRecherche.ForeColor = Color.Black;
            }

        }

        private void textBoxRecherche_Leave(object sender, EventArgs e)
        {
            if (textBoxRecherche.Text == "")
            {
                textBoxRecherche.Text = "Recherche";
                textBoxRecherche.ForeColor = Color.Gray;
            }

        }

        private void UserControlClient_Load(object sender, EventArgs e)
        {
            Actualiserdatagrid();
        }

        private void btnModifierClient_Click(object sender, EventArgs e)
        {
            PL.FRM_Client_Ajouter_Modifier frmclient = new PL.FRM_Client_Ajouter_Modifier(this);
            if (SelectVerif() == null)
            {
                for (int i = 0; i < dvgClient.Rows.Count; i++)
                {
                    object value = dvgClient.Rows[i].Cells[0].Value;
                    if (value != null && (Boolean)value) // si le ligne est selectionner 
                    {
                        frmclient.IdSelect = (int)dvgClient.Rows[i].Cells[1].Value;
                        frmclient.textCIN.Text = dvgClient.Rows[i].Cells[2].Value.ToString();
                        frmclient.textNomClient.Text = dvgClient.Rows[i].Cells[3].Value.ToString();
                        frmclient.textPrenomClient.Text = dvgClient.Rows[i].Cells[4].Value.ToString();
                        frmclient.textAdresseClient.Text = dvgClient.Rows[i].Cells[5].Value.ToString();
                        frmclient.textTELCLIENT.Text = dvgClient.Rows[i].Cells[6].Value.ToString();
                        frmclient.textpermis.Text = dvgClient.Rows[i].Cells[7].Value.ToString();


                    }
                }

                frmclient.LBtitre.Text = "Modifier Client ";
                frmclient.btnActualiser.Visible = false;
                frmclient.ShowDialog();
            }
            else
            {
                MessageBox.Show(SelectVerif(), "Modifier", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void btnsupprimerClient_Click(object sender, EventArgs e)
        {
            BL.CL_Client ClClient = new BL.CL_Client();
            int select = 0;
            // pour supprimr tous les client selectionner 
            for (int i = 0; i < dvgClient.Rows.Count; i++)
            {
                object value = dvgClient.Rows[i].Cells[0].Value;
                if (value != null && (Boolean)value) // si le ligne est selectionner 
                {
                    select++;
                }
            }
            if (select == 0)
            {
                MessageBox.Show("aucun client selectionner", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }
            else
            {
                DialogResult R = MessageBox.Show("Voulez-vous vraiment supprimer ce client", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (R == DialogResult.Yes)
                {   //Pour supprimer toutes les client selectionner
                    for (int j = 0; j < dvgClient.Rows.Count; j++)
                    {
                        object val = dvgClient.Rows[j].Cells[0].Value;
                        if (val != null && (Boolean)val) // si le ligne est selectionner 
                        {
                            ClClient.suprimer_Client(int.Parse(dvgClient.Rows[j].Cells[1].Value.ToString()));
                        }
                    }
                    // actualiser datagrod view
                    Actualiserdatagrid();
                    MessageBox.Show("Suppresion avec succées ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                else
                {
                    MessageBox.Show("Suppresion est annulé ", "suppresion", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }




            }
        }

        private void comboxrecherche_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxRecherche.Enabled = true;
            textBoxRecherche.Text = "";

        }

        private void textBoxRecherche_TextChanged(object sender, EventArgs e)
        {
            bd = new dbLocationContext();
            var listerecherche = bd.Clients.ToList(); // liste des clients 
            if (textBoxRecherche.Text != "") // pas vide
            {
                switch (comboxrecherche.Text)
                {
                    case "CIN":
                        listerecherche = listerecherche.Where(s => s.CIN.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        // stringcomparison.currentcultureignorecase = sois j'ai écrit le premier lettre en maj o minsc
                        // !=-1 existe dans la base de donnée 
                        break;
                    case "Nom":
                        listerecherche = listerecherche.Where(s => s.Nom_client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        // stringcomparison.currentcultureignorecase = sois j'ai écrit le premier lettre en maj o minsc
                        // !=-1 existe dans la base de donnée 
                        break;
                    case "Prenom":
                        listerecherche = listerecherche.Where(s => s.Prenom_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();

                        break;
                    case "Adresse":
                        listerecherche = listerecherche.Where(s => s.Adresse_Client.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    case "Telephone":
                        listerecherche = listerecherche.Where(s => s.tel.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                    
                    case "Permis":
                        listerecherche = listerecherche.Where(s => s.permis.IndexOf(textBoxRecherche.Text, StringComparison.CurrentCultureIgnoreCase) != -1).ToList();
                        break;
                   


                }

            }
            //vider data grid 
            dvgClient.Rows.Clear();
            // ajouter listerecherche dans la datagrid clien
            foreach (var l in listerecherche)
            {
                dvgClient.Rows.Add(false, l.ID_Client, l.CIN, l.Nom_client, l.Prenom_Client, l.Adresse_Client, l.tel, l.permis);
            }

        }
    }
}
