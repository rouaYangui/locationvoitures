﻿
namespace GestionLocationVéhicule.PL
{
    partial class FRM_Client_Ajouter_Modifier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel10 = new System.Windows.Forms.Panel();
            this.textAdresseClient = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textTELCLIENT = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textPrenomClient = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textNomClient = new System.Windows.Forms.TextBox();
            this.LBtitre = new System.Windows.Forms.Label();
            this.btnActualiser = new System.Windows.Forms.Button();
            this.btnEnregistre = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textCIN = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textpermis = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnannuller = new System.Windows.Forms.PictureBox();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).BeginInit();
            this.SuspendLayout();
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gray;
            this.panel10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel10.Location = new System.Drawing.Point(44, 344);
            this.panel10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(413, 1);
            this.panel10.TabIndex = 53;
            // 
            // textAdresseClient
            // 
            this.textAdresseClient.BackColor = System.Drawing.Color.GhostWhite;
            this.textAdresseClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textAdresseClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAdresseClient.ForeColor = System.Drawing.Color.Gray;
            this.textAdresseClient.Location = new System.Drawing.Point(41, 270);
            this.textAdresseClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textAdresseClient.Multiline = true;
            this.textAdresseClient.Name = "textAdresseClient";
            this.textAdresseClient.Size = new System.Drawing.Size(442, 65);
            this.textAdresseClient.TabIndex = 52;
            this.textAdresseClient.Text = "Adresse Client";
            this.textAdresseClient.Enter += new System.EventHandler(this.textAdresseClient_Enter);
            this.textAdresseClient.Leave += new System.EventHandler(this.textAdresseClient_Leave);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gray;
            this.panel7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel7.Location = new System.Drawing.Point(296, 169);
            this.panel7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(188, 1);
            this.panel7.TabIndex = 41;
            // 
            // textTELCLIENT
            // 
            this.textTELCLIENT.BackColor = System.Drawing.Color.GhostWhite;
            this.textTELCLIENT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textTELCLIENT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textTELCLIENT.ForeColor = System.Drawing.Color.Gray;
            this.textTELCLIENT.Location = new System.Drawing.Point(292, 140);
            this.textTELCLIENT.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textTELCLIENT.Multiline = true;
            this.textTELCLIENT.Name = "textTELCLIENT";
            this.textTELCLIENT.Size = new System.Drawing.Size(204, 31);
            this.textTELCLIENT.TabIndex = 39;
            this.textTELCLIENT.Text = "Telephone de Client";
            this.textTELCLIENT.TextChanged += new System.EventHandler(this.textTELCLIENT_TextChanged);
            this.textTELCLIENT.Enter += new System.EventHandler(this.textTELCLIENT_Enter);
            this.textTELCLIENT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textTELCLIENT_KeyPress);
            this.textTELCLIENT.Leave += new System.EventHandler(this.textTELCLIENT_Leave);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Gray;
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel6.Location = new System.Drawing.Point(296, 116);
            this.panel6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(188, 1);
            this.panel6.TabIndex = 38;
            // 
            // textPrenomClient
            // 
            this.textPrenomClient.BackColor = System.Drawing.Color.GhostWhite;
            this.textPrenomClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textPrenomClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textPrenomClient.ForeColor = System.Drawing.Color.Gray;
            this.textPrenomClient.Location = new System.Drawing.Point(292, 84);
            this.textPrenomClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textPrenomClient.Multiline = true;
            this.textPrenomClient.Name = "textPrenomClient";
            this.textPrenomClient.Size = new System.Drawing.Size(204, 31);
            this.textPrenomClient.TabIndex = 36;
            this.textPrenomClient.Text = "Prenom de Client";
            this.textPrenomClient.Enter += new System.EventHandler(this.textPrenomClient_Enter);
            this.textPrenomClient.Leave += new System.EventHandler(this.textPrenomClient_Leave);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel5.Location = new System.Drawing.Point(41, 117);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(188, 1);
            this.panel5.TabIndex = 35;
            // 
            // textNomClient
            // 
            this.textNomClient.BackColor = System.Drawing.Color.GhostWhite;
            this.textNomClient.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNomClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNomClient.ForeColor = System.Drawing.Color.Gray;
            this.textNomClient.Location = new System.Drawing.Point(41, 88);
            this.textNomClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textNomClient.Multiline = true;
            this.textNomClient.Name = "textNomClient";
            this.textNomClient.Size = new System.Drawing.Size(204, 31);
            this.textNomClient.TabIndex = 33;
            this.textNomClient.Text = "Nom de Client";
            this.textNomClient.FontChanged += new System.EventHandler(this.textNomClient_FontChanged);
            this.textNomClient.Enter += new System.EventHandler(this.textNomClient_Enter);
            this.textNomClient.Leave += new System.EventHandler(this.textNomClient_Leave);
            // 
            // LBtitre
            // 
            this.LBtitre.AutoSize = true;
<<<<<<< HEAD
            this.LBtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitre.ForeColor = System.Drawing.Color.White;
            this.LBtitre.Location = new System.Drawing.Point(4, 5);
            this.LBtitre.Name = "LBtitre";
            this.LBtitre.Size = new System.Drawing.Size(171, 29);
            this.LBtitre.TabIndex = 31;
            this.LBtitre.Text = "Ajouter Client";
            // 
=======
            this.LBtitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LBtitre.ForeColor = System.Drawing.Color.Black;
            this.LBtitre.Location = new System.Drawing.Point(155, 22);
            this.LBtitre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LBtitre.Name = "LBtitre";
            this.LBtitre.Size = new System.Drawing.Size(209, 36);
            this.LBtitre.TabIndex = 31;
            this.LBtitre.Text = "Ajouter Client";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::GestionLocationVéhicule.Properties.Resources.adresse;
            this.pictureBox7.Location = new System.Drawing.Point(6, 270);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(29, 31);
            this.pictureBox7.TabIndex = 48;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::GestionLocationVéhicule.Properties.Resources.Phone_32;
            this.pictureBox4.Location = new System.Drawing.Point(258, 140);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(29, 31);
            this.pictureBox4.TabIndex = 40;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox3.Location = new System.Drawing.Point(258, 84);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 31);
            this.pictureBox3.TabIndex = 37;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox2.Location = new System.Drawing.Point(8, 88);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(29, 31);
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::GestionLocationVéhicule.Properties.Resources.Button_Delete_icon1;
            this.btnannuller.Location = new System.Drawing.Point(479, 9);
            this.btnannuller.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(31, 31);
            this.btnannuller.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnannuller.TabIndex = 32;
            this.btnannuller.TabStop = false;
            this.btnannuller.Click += new System.EventHandler(this.btnannuller_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox8.Location = new System.Drawing.Point(8, 141);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(29, 31);
            this.pictureBox8.TabIndex = 50;
            this.pictureBox8.TabStop = false;
            // 
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            // btnActualiser
            // 
            this.btnActualiser.BackColor = System.Drawing.Color.Teal;
            this.btnActualiser.FlatAppearance.BorderSize = 0;
            this.btnActualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnActualiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualiser.ForeColor = System.Drawing.Color.White;
            this.btnActualiser.Location = new System.Drawing.Point(227, 420);
            this.btnActualiser.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnActualiser.Name = "btnActualiser";
            this.btnActualiser.Size = new System.Drawing.Size(125, 39);
            this.btnActualiser.TabIndex = 54;
            this.btnActualiser.Text = "Actualiser";
            this.btnActualiser.UseVisualStyleBackColor = false;
            this.btnActualiser.Click += new System.EventHandler(this.btnActualiser_Click);
            // 
            // btnEnregistre
            // 
            this.btnEnregistre.BackColor = System.Drawing.Color.SeaGreen;
            this.btnEnregistre.FlatAppearance.BorderSize = 0;
            this.btnEnregistre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnregistre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistre.ForeColor = System.Drawing.Color.White;
            this.btnEnregistre.Location = new System.Drawing.Point(368, 420);
            this.btnEnregistre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEnregistre.Name = "btnEnregistre";
            this.btnEnregistre.Size = new System.Drawing.Size(140, 39);
            this.btnEnregistre.TabIndex = 55;
            this.btnEnregistre.Text = "Enregistre ";
            this.btnEnregistre.UseVisualStyleBackColor = false;
            this.btnEnregistre.Click += new System.EventHandler(this.btnEnregistre_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Location = new System.Drawing.Point(41, 170);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(188, 1);
            this.panel1.TabIndex = 57;
            // 
            // textCIN
            // 
            this.textCIN.BackColor = System.Drawing.Color.GhostWhite;
            this.textCIN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCIN.ForeColor = System.Drawing.Color.Gray;
<<<<<<< HEAD
            this.textCIN.Location = new System.Drawing.Point(55, 173);
            this.textCIN.MaxLength = 3000;
=======
            this.textCIN.Location = new System.Drawing.Point(41, 141);
            this.textCIN.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            this.textCIN.Multiline = true;
            this.textCIN.Name = "textCIN";
            this.textCIN.Size = new System.Drawing.Size(204, 31);
            this.textCIN.TabIndex = 56;
            this.textCIN.Text = "Numéro de carte d\'identité";
            this.textCIN.Enter += new System.EventHandler(this.textCIN_Enter);
            this.textCIN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textCIN_KeyPress);
            this.textCIN.Leave += new System.EventHandler(this.textCIN_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel2.Location = new System.Drawing.Point(41, 231);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(188, 1);
            this.panel2.TabIndex = 60;
            // 
            // textpermis
            // 
            this.textpermis.BackColor = System.Drawing.Color.GhostWhite;
            this.textpermis.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textpermis.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textpermis.ForeColor = System.Drawing.Color.Gray;
            this.textpermis.Location = new System.Drawing.Point(41, 202);
            this.textpermis.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textpermis.Multiline = true;
            this.textpermis.Name = "textpermis";
            this.textpermis.Size = new System.Drawing.Size(204, 31);
            this.textpermis.TabIndex = 59;
            this.textpermis.Text = "Numéro de permis";
            this.textpermis.Enter += new System.EventHandler(this.textpermis_Enter);
            this.textpermis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textpermis_KeyPress);
            this.textpermis.Leave += new System.EventHandler(this.textpermis_Leave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.panel3.Controls.Add(this.LBtitre);
            this.panel3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Location = new System.Drawing.Point(1, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(643, 38);
            this.panel3.TabIndex = 61;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox1.Location = new System.Drawing.Point(8, 202);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 31);
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox8.Location = new System.Drawing.Point(10, 173);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(39, 38);
            this.pictureBox8.TabIndex = 50;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::GestionLocationVéhicule.Properties.Resources.adresse;
            this.pictureBox7.Location = new System.Drawing.Point(8, 332);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(39, 38);
            this.pictureBox7.TabIndex = 48;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::GestionLocationVéhicule.Properties.Resources.Phone_32;
            this.pictureBox4.Location = new System.Drawing.Point(344, 172);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 38);
            this.pictureBox4.TabIndex = 40;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox3.Location = new System.Drawing.Point(344, 103);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(39, 38);
            this.pictureBox3.TabIndex = 37;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::GestionLocationVéhicule.Properties.Resources.login_32;
            this.pictureBox2.Location = new System.Drawing.Point(10, 108);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(39, 38);
            this.pictureBox2.TabIndex = 34;
            this.pictureBox2.TabStop = false;
            // 
            // btnannuller
            // 
            this.btnannuller.Image = global::GestionLocationVéhicule.Properties.Resources.delete_symbol__1_;
            this.btnannuller.Location = new System.Drawing.Point(644, 3);
            this.btnannuller.Name = "btnannuller";
            this.btnannuller.Size = new System.Drawing.Size(41, 38);
            this.btnannuller.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.btnannuller.TabIndex = 32;
            this.btnannuller.TabStop = false;
            this.btnannuller.Click += new System.EventHandler(this.btnannuller_Click);
            // 
            // FRM_Client_Ajouter_Modifier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
<<<<<<< HEAD
            this.ClientSize = new System.Drawing.Size(689, 594);
            this.Controls.Add(this.panel3);
=======
            this.ClientSize = new System.Drawing.Size(517, 483);
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textpermis);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.textCIN);
            this.Controls.Add(this.btnEnregistre);
            this.Controls.Add(this.btnActualiser);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.textAdresseClient);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.textTELCLIENT);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.textPrenomClient);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.textNomClient);
            this.Controls.Add(this.btnannuller);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FRM_Client_Ajouter_Modifier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FRM_Client_Ajouter_Modifier";
            this.Load += new System.EventHandler(this.FRM_Client_Ajouter_Modifier_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnannuller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel10;
        public System.Windows.Forms.TextBox textAdresseClient;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.TextBox textTELCLIENT;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.TextBox textPrenomClient;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox2;
        public System.Windows.Forms.TextBox textNomClient;
        private System.Windows.Forms.PictureBox btnannuller;
        public System.Windows.Forms.Label LBtitre;
        private System.Windows.Forms.PictureBox pictureBox8;
        public System.Windows.Forms.Button btnActualiser;
        private System.Windows.Forms.Button btnEnregistre;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox textCIN;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.TextBox textpermis;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
    }
}