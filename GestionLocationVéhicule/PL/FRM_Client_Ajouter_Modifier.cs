﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionLocationVéhicule.PL
{
    public partial class FRM_Client_Ajouter_Modifier : Form
    {
        private UserControl usrClient;
        public FRM_Client_Ajouter_Modifier(UserControl usrC)
        {
            InitializeComponent();
            this.usrClient = usrC;
        }

        private void FRM_Client_Ajouter_Modifier_Load(object sender, EventArgs e)
        {

        }
        string TestObligatoire()
        {
            if (textNomClient.Text == "" || textNomClient.Text == "Nom de Client")
            {
                textNomClient.Focus();
                return "Entrer Le Nom de Client";
                
            }
            if (textPrenomClient.Text == "" || textPrenomClient.Text == "Prenom de Client")
            {
                textPrenomClient.Focus();
                return "Entrer Le Prenom de Client ";
            }
            if (textAdresseClient.Text == "" || textAdresseClient.Text == "Adresse Client")
            {
                textAdresseClient.Focus();
                return "Entrer L'adresse Client ";
            }
            if (textTELCLIENT.Text == "Telephone de Client" || textTELCLIENT.Text == "Telephone de Client")
            {
                textTELCLIENT.Focus();
                return "Entrer Le Telephone de Client ";
            }
            
            if (textpermis.Text == "" || textpermis.Text == "Numéro de permis")
            {
                textpermis.Focus();
                return "Entrer numéro de permis ";
            }
            if (textCIN.Text == "" || textCIN.Text == "Numéro de carte d'identité")
            {
                textCIN.Focus();
                return "Entrer numéro de carte d'identité";
            }
            //verifier email valide ou nom 
           
            return null;
        }



        private void btnannuller_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textNomClient_Enter(object sender, EventArgs e)
        {
            if (textNomClient.Text == "Nom de Client")
            {
                textNomClient.Text = "";
                textNomClient.ForeColor = Color.Black;
            }

        }

        private void textNomClient_Leave(object sender, EventArgs e)
        {
            if (textNomClient.Text == "")
            {
                textNomClient.Text = "Nom de Client";
                textNomClient.ForeColor = Color.Gray;
            }

        }

        private void textPrenomClient_Enter(object sender, EventArgs e)
        {
            if (textPrenomClient.Text == "Prenom de Client")
            {
                textPrenomClient.Text = "";
                textPrenomClient.ForeColor = Color.Black;
            }

        }

        private void textPrenomClient_Leave(object sender, EventArgs e)
        {
            if (textPrenomClient.Text == "")
            {
                textPrenomClient.Text = "Prenom de Client";
                textPrenomClient.ForeColor = Color.Gray;
            }

        }

       
        private void textTELCLIENT_Enter(object sender, EventArgs e)
        {
            if (textTELCLIENT.Text == "Telephone de Client")
            {
                textTELCLIENT.Text = "";
                textTELCLIENT.ForeColor = Color.Black;
            }

        }

        private void textTELCLIENT_Leave(object sender, EventArgs e)
        {
            if (textTELCLIENT.Text == "")
            {
                textTELCLIENT.Text = "Telephone de Client";
                textTELCLIENT.ForeColor = Color.Gray;
            }

        }

        private void textAdresseClient_Enter(object sender, EventArgs e)
        {
            if (textAdresseClient.Text == "Adresse Client")
            {
                textAdresseClient.Text = "";
                textAdresseClient.ForeColor = Color.Black;
            }

          

        }

        private void textAdresseClient_Leave(object sender, EventArgs e)
        {
            if (textAdresseClient.Text == "")
            {
                textAdresseClient.Text = "Adresse Client";
                textAdresseClient.ForeColor = Color.Gray;
            }
        }

        private void textCIN_Enter(object sender, EventArgs e)
        {
            if (textCIN.Text == "Numéro de carte d'identité")
            {
                textCIN.Text = "";
                textCIN.ForeColor = Color.Black;
            }
        }

        private void textCIN_Leave(object sender, EventArgs e)
        {

            if (textCIN.Text == "")
            {
                textCIN.Text = "Numéro de carte d'identité";
                textCIN.ForeColor = Color.Gray;
            }
        }

        private void textpermis_Enter(object sender, EventArgs e)
        {
            if (textpermis.Text == "Numéro de permis")
            {
                textpermis.Text = "";
                textpermis.ForeColor = Color.Black;
            }
        }

        private void textpermis_Leave(object sender, EventArgs e)
        {
            if (textpermis.Text == "")
            {
                textpermis.Text = "Numéro de permis";
                textpermis.ForeColor = Color.Gray;
            }
        }

        private void textTELCLIENT_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }

        }

        private void textCIN_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }

        }

        private void textpermis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 75) // asci des numéro
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }


        }

        private void btnActualiser_Click(object sender, EventArgs e)
        {
            textNomClient.Text = "Nom de Client";
            textNomClient.ForeColor = Color.Gray;

            textPrenomClient.Text = "Prenom de Client";
            textPrenomClient.ForeColor = Color.Gray;

            textAdresseClient.Text = "Adresse Client";
            textAdresseClient.ForeColor = Color.Gray;

            textTELCLIENT.Text = "Telephone de Client";
            textTELCLIENT.ForeColor = Color.Gray;

            

            textCIN.Text = "Pays Client";
            textCIN.ForeColor = Color.Gray;

            textpermis.Text = "Ville  Client";
            textpermis.ForeColor = Color.Gray;

        }
        public int IdSelect;
        private void btnEnregistre_Click(object sender, EventArgs e)
        {
             if(TestObligatoire() != null)
            {
                MessageBox.Show(TestObligatoire(),"Obligatoire",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {   if(LBtitre.Text== "Ajouter Client")
                {
                    BL.CL_Client ClClient = new BL.CL_Client(); // créer un objet pour stock dans BD
                    if (ClClient.Ajouter_Client(textNomClient.Text, textPrenomClient.Text, textAdresseClient.Text, textTELCLIENT.Text, textCIN.Text,textpermis.Text))
                    {
                        MessageBox.Show("Client Ajouter avec succes", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        (usrClient as UserControlClient).Actualiserdatagrid();
                    }
                    else
                    {
                        MessageBox.Show("Nom et Prenom de client déjà existant", "Ajouter", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else // lb=modifier 
                {
                    BL.CL_Client ClClient = new BL.CL_Client();
                    UserControlClient user = new UserControlClient();
                    DialogResult R = MessageBox.Show("Voulez-vous vraiment modifier ce client", "Modifier", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if( R== DialogResult.Yes)
                    {
                        ClClient.modifier_client(IdSelect, textNomClient.Text, textPrenomClient.Text, textAdresseClient.Text, textTELCLIENT.Text,textCIN.Text, textpermis.Text);
                        // pour actualiser  datagrid view
                        (usrClient as UserControlClient).Actualiserdatagrid();
                        MessageBox.Show("Client Modifier avec succés","Modification", MessageBoxButtons.OK,MessageBoxIcon.Asterisk);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Modification est anullé ", "Modification ",MessageBoxButtons.OK , MessageBoxIcon.Warning) ;
                    }
                    
                }
            }

        }

        private void textNomClient_FontChanged(object sender, EventArgs e)
        {

        }
    }
}
