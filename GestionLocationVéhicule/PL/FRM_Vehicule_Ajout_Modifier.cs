﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GestionLocationVéhicule.PL
{
    public partial class FRM_Vehicule_Ajout_Modifier : Form
    {
        public FRM_Vehicule_Ajout_Modifier()
        {
            InitializeComponent();
        }
        string testObligatoire()
        {
            if (textmatricule.Text == "Matricule voiture" || textmatricule.Text == "")
            {
                return "Le matricule du voiture ! ";
            }
            if (textmarque.Text == "Marque voiture" || textmarque.Text == "")
            {
                return " Donner la marque du voiture "; 
            }
            if (textmodele.Text == "Modele" || textmodele.Text =="")
            {
                return " Donner le modele du voiture "; 
            }
            if (textprix.Text == "Prix/Jour" || textprix.Text == "")
            {
                return " Donner le prix par jour du voiture ";
            }
            return null;
        }
        private void FRM_Vehicule_Ajout_Modifier_Load(object sender, EventArgs e)
        {

        }

        private void textmatricule_Enter(object sender, EventArgs e)
        {
            if (textmatricule.Text == "Matricule voiture")
            {
                textmatricule.Text = "";
                textmatricule.ForeColor = Color.Black;
            }
        }
        private void textmatricule_Leave_1(object sender, EventArgs e)
        {
            if (textmatricule.Text == "")
            {
                textmatricule.Text = "Matricule voiture";
                textmatricule.ForeColor = Color.Silver;
            }
        }


        private void textmarque_Enter(object sender, EventArgs e)
        {
            if (textmarque.Text == "Marque voiture")
            {
                textmarque.Text = "";
                textmarque.ForeColor = Color.Black;
            }

        }
        private void textmarque_Leave(object sender, EventArgs e)
        {
            if (textmarque.Text == "")
            {
                textmarque.Text = "Marque voiture";
                textmarque.ForeColor = Color.Silver;
            }

        }

        private void textmodele_Enter(object sender, EventArgs e)
        {
            if (textmodele.Text == "Modele")
            {
                textmodele.Text = "";
                textmodele.ForeColor = Color.Black;
            }
        }
        private void textmodele_Leave(object sender, EventArgs e)
        {
            if (textmodele.Text == "")
            {
                textmodele.Text = "Modele";
                textmodele.ForeColor = Color.Silver;
            }

        }
        private void textkilo_Enter(object sender, EventArgs e)
        {
            if (textkilo.Text == "Kilometrage")
            {
                textkilo.Text = "";
                textkilo.ForeColor = Color.Black;
            }
        }

        private void textprix_Enter(object sender, EventArgs e)
        {
            if (textprix.Text == "Prix/Jour")
            {
                textprix.Text = "";
                textprix.ForeColor = Color.Black;
            }
        }

        private void textkilo_Leave(object sender, EventArgs e)
        {
            if (textkilo.Text == "")
            {
                textkilo.Text = "Kilometrage";
                textkilo.ForeColor = Color.Silver;
            }
        }

        private void textprix_Leave(object sender, EventArgs e)
        {
            if (textprix.Text == "")
            {
                textprix.Text = "Prix/Jour";
                textprix.ForeColor = Color.Silver;
            }
        }

        private void btnquite_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnParcourire_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = " |*.JPG , *.PNG, *.GIF , *.BMP, *.JPEG";

            if (op.ShowDialog()== DialogResult.OK)
            {
                imagebox.Image = Image.FromFile(op.FileName);
            }
        }

        private void btnActualiser_Click(object sender, EventArgs e)
        {
            //vider
            textmatricule.Text = "Matricule voiture"; textmatricule.ForeColor = Color.Silver;
            textmarque.Text = "Marque voiture"; textmarque.ForeColor = Color.Silver;
            textmodele.Text = "Modele"; textmodele.ForeColor = Color.Silver;
            textkilo.Text = "Kilometrage"; textkilo.ForeColor = Color.Silver;
            textprix.Text = "Prix/Jour"; textprix.ForeColor = Color.Silver;
            imagebox.Image = null; 
        }

        private void btnEnregistre_Click(object sender, EventArgs e)
        {
            if (testObligatoire()!= null)
            {
                MessageBox.Show(testObligatoire(), "Obligatoire", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (LBtitre.Text =="Ajouter Voiture")
                {
                    BL.CL_Vehicule clv = new BL.CL_Vehicule();
                    MemoryStream mr = new MemoryStream();
                    imagebox.Image.Save(mr, imagebox.Image.RawFormat);
                    byte[] img = mr.ToArray();
                    if (clv.Ajouter_Vehicule(textmatricule.Text, textmodele.Text, textmarque.Text, textkilo.Text, textprix.Text ,img))
                    {
                        //MessageBox.Show("Ajoutee avec succes ", )
                        }
                }
            }
        }

        private void textmatricule_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }

        private void textprix_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
            {
                e.Handled = true;
            }
            if (e.KeyChar == 8)
            {
                e.Handled = false;
            }
        }
    }
}
