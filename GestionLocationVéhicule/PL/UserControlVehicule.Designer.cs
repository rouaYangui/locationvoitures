﻿
namespace GestionLocationVéhicule.PL
{
    partial class UserControlVehicule
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dvVehicule = new System.Windows.Forms.DataGridView();
            this.Colimn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboxrecherche = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxRecherche = new System.Windows.Forms.TextBox();
            this.btnphoto = new System.Windows.Forms.Button();
            this.btnsupprimerVehicule = new System.Windows.Forms.Button();
            this.btnModifierVehicule = new System.Windows.Forms.Button();
            this.btnAjouterVehicule = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dvVehicule)).BeginInit();
            this.SuspendLayout();
            // 
            // dvVehicule
            // 
            this.dvVehicule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dvVehicule.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dvVehicule.BackgroundColor = System.Drawing.Color.GhostWhite;
            this.dvVehicule.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvVehicule.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dvVehicule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvVehicule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Colimn1,
            this.Column6,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
<<<<<<< HEAD
            this.dvgClient.EnableHeadersVisualStyles = false;
            this.dvgClient.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dvgClient.Location = new System.Drawing.Point(32, 234);
            this.dvgClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dvgClient.Name = "dvgClient";
            this.dvgClient.RowHeadersVisible = false;
            this.dvgClient.RowHeadersWidth = 51;
            this.dvgClient.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dvgClient.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dvgClient.RowTemplate.Height = 24;
            this.dvgClient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvgClient.Size = new System.Drawing.Size(984, 327);
            this.dvgClient.TabIndex = 24;
=======
            this.dvVehicule.EnableHeadersVisualStyles = false;
            this.dvVehicule.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dvVehicule.Location = new System.Drawing.Point(24, 190);
            this.dvVehicule.Margin = new System.Windows.Forms.Padding(2);
            this.dvVehicule.Name = "dvVehicule";
            this.dvVehicule.RowHeadersVisible = false;
            this.dvVehicule.RowHeadersWidth = 51;
            this.dvVehicule.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.dvVehicule.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dvVehicule.RowTemplate.Height = 24;
            this.dvVehicule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dvVehicule.Size = new System.Drawing.Size(845, 266);
            this.dvVehicule.TabIndex = 24;
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            // 
            // Colimn1
            // 
            this.Colimn1.HeaderText = "Select";
            this.Colimn1.MinimumWidth = 6;
            this.Colimn1.Name = "Colimn1";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Matricule";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Marque";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Modele";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Kilométrage";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Prix/Jour";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Etat";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // comboxrecherche
            // 
            this.comboxrecherche.BackColor = System.Drawing.Color.GhostWhite;
            this.comboxrecherche.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboxrecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboxrecherche.FormattingEnabled = true;
            this.comboxrecherche.Items.AddRange(new object[] {
<<<<<<< HEAD
            "CIN",
            "Nom",
            "Prenom",
            "Telephone",
            "Permis"});
            this.comboxrecherche.Location = new System.Drawing.Point(191, 144);
            this.comboxrecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboxrecherche.Name = "comboxrecherche";
            this.comboxrecherche.Size = new System.Drawing.Size(267, 37);
=======
            "Matricule",
            "Marque",
            "Modele"});
            this.comboxrecherche.Location = new System.Drawing.Point(129, 120);
            this.comboxrecherche.Margin = new System.Windows.Forms.Padding(2);
            this.comboxrecherche.Name = "comboxrecherche";
            this.comboxrecherche.Size = new System.Drawing.Size(215, 33);
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            this.comboxrecherche.TabIndex = 23;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gray;
            this.panel3.Location = new System.Drawing.Point(529, 186);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(400, 2);
            this.panel3.TabIndex = 22;
            // 
            // textBoxRecherche
            // 
            this.textBoxRecherche.BackColor = System.Drawing.Color.GhostWhite;
            this.textBoxRecherche.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxRecherche.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRecherche.ForeColor = System.Drawing.Color.Gray;
            this.textBoxRecherche.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBoxRecherche.Location = new System.Drawing.Point(528, 138);
            this.textBoxRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxRecherche.Multiline = true;
            this.textBoxRecherche.Name = "textBoxRecherche";
            this.textBoxRecherche.Size = new System.Drawing.Size(400, 47);
            this.textBoxRecherche.TabIndex = 21;
            this.textBoxRecherche.Text = "Recherche";
            this.textBoxRecherche.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
<<<<<<< HEAD
            // btnsupprimerClient
            // 
            this.btnsupprimerClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsupprimerClient.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnsupprimerClient.FlatAppearance.BorderSize = 0;
            this.btnsupprimerClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsupprimerClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsupprimerClient.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnsupprimerClient.Image = global::GestionLocationVéhicule.Properties.Resources.Close_2_icon;
            this.btnsupprimerClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsupprimerClient.Location = new System.Drawing.Point(721, 37);
            this.btnsupprimerClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnsupprimerClient.Name = "btnsupprimerClient";
            this.btnsupprimerClient.Size = new System.Drawing.Size(295, 53);
            this.btnsupprimerClient.TabIndex = 20;
            this.btnsupprimerClient.Text = "Supprimer";
            this.btnsupprimerClient.UseVisualStyleBackColor = false;
            // 
            // btnModifierClient
            // 
            this.btnModifierClient.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnModifierClient.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnModifierClient.FlatAppearance.BorderSize = 0;
            this.btnModifierClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifierClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierClient.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnModifierClient.Image = global::GestionLocationVéhicule.Properties.Resources.Recycle_iconaaa;
            this.btnModifierClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModifierClient.Location = new System.Drawing.Point(375, 37);
            this.btnModifierClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnModifierClient.Name = "btnModifierClient";
            this.btnModifierClient.Size = new System.Drawing.Size(323, 52);
            this.btnModifierClient.TabIndex = 19;
            this.btnModifierClient.Text = "Modifier";
            this.btnModifierClient.UseVisualStyleBackColor = false;
            // 
            // btnAjouterClient
            // 
            this.btnAjouterClient.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAjouterClient.FlatAppearance.BorderSize = 0;
            this.btnAjouterClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterClient.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAjouterClient.Image = global::GestionLocationVéhicule.Properties.Resources.Actions_list_add_icon;
            this.btnAjouterClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAjouterClient.Location = new System.Drawing.Point(32, 38);
            this.btnAjouterClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAjouterClient.Name = "btnAjouterClient";
            this.btnAjouterClient.Size = new System.Drawing.Size(299, 53);
            this.btnAjouterClient.TabIndex = 18;
            this.btnAjouterClient.Text = "Ajouter";
            this.btnAjouterClient.UseVisualStyleBackColor = false;
            this.btnAjouterClient.Click += new System.EventHandler(this.btnAjouterClient_Click);
            // 
            // UserControlVehicule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
=======
            // btnphoto
            // 
            this.btnphoto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnphoto.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnphoto.FlatAppearance.BorderSize = 0;
            this.btnphoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnphoto.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnphoto.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnphoto.Image = global::GestionLocationVéhicule.Properties.Resources.Pictures_icon;
            this.btnphoto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnphoto.Location = new System.Drawing.Point(637, 33);
            this.btnphoto.Margin = new System.Windows.Forms.Padding(2);
            this.btnphoto.Name = "btnphoto";
            this.btnphoto.Size = new System.Drawing.Size(247, 42);
            this.btnphoto.TabIndex = 25;
            this.btnphoto.Text = "   Afficher Photo";
            this.btnphoto.UseVisualStyleBackColor = false;
            this.btnphoto.Click += new System.EventHandler(this.btnphoto_Click);
            // 
            // btnsupprimerVehicule
            // 
            this.btnsupprimerVehicule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnsupprimerVehicule.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnsupprimerVehicule.FlatAppearance.BorderSize = 0;
            this.btnsupprimerVehicule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsupprimerVehicule.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsupprimerVehicule.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnsupprimerVehicule.Image = global::GestionLocationVéhicule.Properties.Resources.Close_2_icon;
            this.btnsupprimerVehicule.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsupprimerVehicule.Location = new System.Drawing.Point(432, 32);
            this.btnsupprimerVehicule.Margin = new System.Windows.Forms.Padding(2);
            this.btnsupprimerVehicule.Name = "btnsupprimerVehicule";
            this.btnsupprimerVehicule.Size = new System.Drawing.Size(183, 43);
            this.btnsupprimerVehicule.TabIndex = 20;
            this.btnsupprimerVehicule.Text = "     Supprimer";
            this.btnsupprimerVehicule.UseVisualStyleBackColor = false;
            // 
            // btnModifierVehicule
            // 
            this.btnModifierVehicule.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnModifierVehicule.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnModifierVehicule.FlatAppearance.BorderSize = 0;
            this.btnModifierVehicule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModifierVehicule.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierVehicule.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnModifierVehicule.Image = global::GestionLocationVéhicule.Properties.Resources.Recycle_iconaaa;
            this.btnModifierVehicule.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModifierVehicule.Location = new System.Drawing.Point(214, 32);
            this.btnModifierVehicule.Margin = new System.Windows.Forms.Padding(2);
            this.btnModifierVehicule.Name = "btnModifierVehicule";
            this.btnModifierVehicule.Size = new System.Drawing.Size(200, 42);
            this.btnModifierVehicule.TabIndex = 19;
            this.btnModifierVehicule.Text = "   Modifier";
            this.btnModifierVehicule.UseVisualStyleBackColor = false;
            // 
            // btnAjouterVehicule
            // 
            this.btnAjouterVehicule.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAjouterVehicule.FlatAppearance.BorderSize = 0;
            this.btnAjouterVehicule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAjouterVehicule.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterVehicule.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAjouterVehicule.Image = global::GestionLocationVéhicule.Properties.Resources.Actions_list_add_icon;
            this.btnAjouterVehicule.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAjouterVehicule.Location = new System.Drawing.Point(24, 31);
            this.btnAjouterVehicule.Margin = new System.Windows.Forms.Padding(2);
            this.btnAjouterVehicule.Name = "btnAjouterVehicule";
            this.btnAjouterVehicule.Size = new System.Drawing.Size(170, 43);
            this.btnAjouterVehicule.TabIndex = 18;
            this.btnAjouterVehicule.Text = "    Ajouter";
            this.btnAjouterVehicule.UseVisualStyleBackColor = false;
            this.btnAjouterVehicule.Click += new System.EventHandler(this.btnAjouterVehicule_Click);
            // 
            // UserControlVehicule
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.Controls.Add(this.btnphoto);
            this.Controls.Add(this.dvVehicule);
            this.Controls.Add(this.comboxrecherche);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.textBoxRecherche);
<<<<<<< HEAD
            this.Controls.Add(this.btnsupprimerClient);
            this.Controls.Add(this.btnModifierClient);
            this.Controls.Add(this.btnAjouterClient);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "UserControlVehicule";
            this.Size = new System.Drawing.Size(1145, 577);
=======
            this.Controls.Add(this.btnsupprimerVehicule);
            this.Controls.Add(this.btnModifierVehicule);
            this.Controls.Add(this.btnAjouterVehicule);
            this.Name = "UserControlVehicule";
            this.Size = new System.Drawing.Size(903, 469);
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
            this.Load += new System.EventHandler(this.UserControlVehicule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dvVehicule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dvVehicule;
        private System.Windows.Forms.ComboBox comboxrecherche;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxRecherche;
        private System.Windows.Forms.Button btnsupprimerVehicule;
        private System.Windows.Forms.Button btnModifierVehicule;
        private System.Windows.Forms.Button btnAjouterVehicule;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Colimn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Button btnphoto;
    }
}
