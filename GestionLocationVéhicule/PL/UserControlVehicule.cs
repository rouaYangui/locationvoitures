﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO; 

namespace GestionLocationVéhicule.PL
{
    public partial class UserControlVehicule : UserControl
    {
        private dbLocationContext bd; 

        private static UserControlVehicule UserVehicule;

        public static UserControlVehicule Instance
        {
            get
            {
                if (UserVehicule== null)
                {
                    UserVehicule = new UserControlVehicule();
                }
                return UserVehicule; 
            }
        }
        public UserControlVehicule()
        {
            InitializeComponent();
            bd = new dbLocationContext();
            // desactiver textbox de recherche 
            textBoxRecherche.Enabled = false;
        }

        public string SelectVerif()
        {

            int Nombreligneselect = 0;

            for (int i = 0; i < dvVehicule.Rows.Count; i++)
            {
                object value = dvVehicule.Rows[i].Cells[0].Value;

                if (value != null && (Boolean)value)
                {
                    Nombreligneselect++;

                }
            }
            if (Nombreligneselect == 0)
            {
                return " Selectionner voiture";

            }
            if (Nombreligneselect > 1)
            {
                return " Selectionner une Voiture  ";

            }
            return null;

        }

        private void UserControlVehicule_Load(object sender, EventArgs e)
        {

        }

<<<<<<< HEAD
        private void btnAjouterClient_Click(object sender, EventArgs e)
        {

=======
        private void btnphoto_Click(object sender, EventArgs e)
        {
            Vehicule v = new Vehicule();
            if (SelectVerif()!=null)
            {
                MessageBox.Show(SelectVerif(), "Selectionner ", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                for (int i=0; i<dvVehicule.Rows.Count; i++)
                {
                    if ((bool)dvVehicule.Rows[i].Cells[0].Value == true)
                    {
                        int IDSelect =(int) dvVehicule.Rows[i].Cells[1].Value;
                        v = bd.Vehicules.SingleOrDefault(s => s.ID_Vehicule == IDSelect);
                        if (v!=null)
                        {
                            Frm_PhotoVoiture frmp = new Frm_PhotoVoiture();
                            MemoryStream MS = new MemoryStream(v.imageVoiture);
                            frmp.ImageVoiture.Image = Image.FromStream(MS);
                            frmp.VoitureNom.Text = dvVehicule.Rows[i].Cells[2].Value.ToString()+" " + dvVehicule.Rows[i].Cells[3].Value.ToString();
                            frmp.ShowDialog();
                        }
                    }
                }
                
            }
        }

        private void btnAjouterVehicule_Click(object sender, EventArgs e)
        {
            PL.FRM_Vehicule_Ajout_Modifier frmVehicule = new FRM_Vehicule_Ajout_Modifier();
            frmVehicule.ShowDialog();
>>>>>>> c0b907eb0c6352c53420b86ea6f981594edd710b
        }
    }
}
